package com.example.examencel;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private EditText txtUser, txtPass;

    private Button btnEntrar, btnSalir;
    private String user,pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        txtUser = (EditText) findViewById(R.id.txtUser);
        txtPass = (EditText) findViewById(R.id.txtPass);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        user = getString(R.string.user);
        pass = getString(R.string.pass);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtUser.getText().toString().matches("") || txtPass.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,
                            "Debe ingresar un usuario y contrasena",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    if (txtUser.getText().toString().matches(user) && txtPass.getText().toString().matches(pass)) {
                        Intent intent = new Intent(getApplicationContext(),
                                CuentaBancoActivity.class);
                        intent.putExtra("cliente", txtUser.getText());
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(MainActivity.this,
                                "Usuario o contrasena incorrectos",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

}