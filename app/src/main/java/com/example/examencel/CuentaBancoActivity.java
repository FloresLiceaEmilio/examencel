package com.example.examencel;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.ArrayList;

public class CuentaBancoActivity extends AppCompatActivity {

    private TextView lblNombre, lblSaldo;
    private EditText txtNumCuenta, txtNombre, txtBanco, txtSaldo, txtCantidad;
    private Button btnConsultar, btnLimpiar, btnSalir;
    private Spinner sp;
    private int pos = 0 ;

    private CuentaBanco CB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cuenta_banco);
        CB = new CuentaBanco();

        sp = findViewById(R.id.spinner);
        iniciarComponentes();

        btnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().isEmpty()
                        || txtBanco.getText().toString().isEmpty()
                        || txtNumCuenta.getText().toString().isEmpty()
                        || txtSaldo.getText().toString().isEmpty()
                        || txtCantidad.getText().toString().isEmpty()){
                    Toast.makeText(CuentaBancoActivity.this, "Debe llenar todos los datos", Toast.LENGTH_SHORT).show();
                }
                else {
                    float saldo = 0.0f;
                    CB.setNombre(txtNombre.getText().toString());
                    CB.setBanco(txtBanco.getText().toString());
                    CB.setNumCuenta(txtNumCuenta.getText().toString());
                    CB.setSaldo(Float.parseFloat(txtSaldo.getText().toString()));

                    switch (pos){
                        case 0 : //consultar
                            lblSaldo.setText(String.valueOf("Saldo: " + CB.getSaldo()));

                            break;
                        case 1 : //Retirar
                            float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                            if(cantidad>CB.getSaldo()){
                                Toast.makeText(CuentaBancoActivity.this, "Saldo insuficiente", Toast.LENGTH_SHORT).show();
                                break;
                            }else {
                                saldo = CB.retirarDinero(Float.parseFloat(txtCantidad.getText().toString()));

                                lblSaldo.setText(String.valueOf("Nuevo Saldo:" + saldo));
                                txtSaldo.setText(String.valueOf(CB.getSaldo()));
                                txtSaldo.setText(String.valueOf(saldo));
                                break;
                            }
                        case 2 : //Depositar

                            saldo = CB.depositarDinero(Float.parseFloat(txtCantidad.getText().toString()));
                            lblSaldo.setText(String.valueOf("Nuevo Saldo:" + saldo));
                            txtSaldo.setText(String.valueOf(CB.getSaldo()));
                            txtSaldo.setText(String.valueOf(saldo));
                            break;
                    }


                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNombre.setText("");
                txtBanco.setText("");
                txtNumCuenta.setText("");
                txtSaldo.setText("");
                txtCantidad.setText("");
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        lblSaldo = (TextView) findViewById(R.id.lblSaldo);
        txtBanco = (EditText) findViewById(R.id.txtBanco);
        txtNumCuenta = (EditText) findViewById(R.id.txtNumCuenta);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        btnConsultar = (Button) findViewById(R.id.btnAplicar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        lblNombre.setText(nombre);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.movimientos));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);
    }

}