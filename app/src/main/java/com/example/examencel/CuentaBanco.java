package com.example.examencel;

import java.io.Serializable;

public class CuentaBanco implements Serializable {

    private String numCuenta;
    private String nombre;
    private String Banco;
    private float saldo;


    public CuentaBanco(){
        this.nombre = "";
        this.numCuenta = "";
        this.Banco = "";
        this.saldo = 0;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBanco() {
        return Banco;
    }

    public void setBanco(String banco) {
        Banco = banco;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public void obtenerSaldo(){
        saldo = this.saldo;
        System.out.println("El saldo de la cuenta es: " + saldo);
        return;
    }

    public float depositarDinero(float cantidad){

        return this.saldo+cantidad;
    }

    public float retirarDinero(float cantidad){
        return this.saldo-cantidad;
    }

}
